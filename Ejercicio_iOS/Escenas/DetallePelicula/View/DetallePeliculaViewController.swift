//
//  DetallePeliculaViewController.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 16/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class DetallePeliculaViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var ivImagenFondo: UIImageView!
    @IBOutlet weak var lbTituloPelicula: UILabel!
    @IBOutlet weak var tvDetallePelicula: UITextView!
    @IBOutlet weak var lbTituloOriginal: UILabel!
    @IBOutlet weak var lblFechaLanzamiento: UILabel!
    @IBOutlet weak var lbPopularidad: UILabel!
    @IBOutlet weak var lbPromedio: UILabel!
    @IBOutlet weak var lbTotalVotos: UILabel!
    @IBOutlet var viewDetalle: UIView!
    @IBOutlet var scrollDetalle : UIScrollView!
    
    //Variables
    var pelicula = Pelicula.results()
    var heightDetalle : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heightDetalle = tvDetallePelicula.frame.size.height
        configurarNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configurarScroll()
        configurarVista()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(tvDetallePelicula.frame.height > heightDetalle){
            let diferencia = tvDetallePelicula.frame.height - heightDetalle
            viewDetalle.frame = CGRect.init(x: tvDetallePelicula.frame.origin.x, y: tvDetallePelicula.frame.origin.y, width: tvDetallePelicula.frame.width, height: viewDetalle.frame.height + diferencia)
            configurarScroll()
        }
    }
    
    func configurarNavigation(){
        self.title = "Detalle"
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backItem
    }
    
    func configurarScroll(){
        let ancho = UIScreen.main.bounds.size.width
        
        self.scrollDetalle.frame = CGRect.init(x: scrollDetalle.frame.origin.x , y: scrollDetalle.frame.origin.y, width: ancho, height: scrollDetalle.frame.height)
        self.viewDetalle.frame = CGRect.init(x: 0, y: 0, width: ancho - 30, height: viewDetalle.frame.height)
        
        self.scrollDetalle.addSubview(viewDetalle)
        self.scrollDetalle.contentSize = viewDetalle.frame.size
    }
    
    func configurarVista(){
        var pathImage = Utilerias.obtenerDocumentsDirectory()
        pathImage.append(String(format: "/PortadasPeliculas/backdrop_path_%d.jpg", pelicula.id))
        self.ivImagenFondo.image = UIImage.init(contentsOfFile: pathImage)
        
        self.lbTituloPelicula.text = pelicula.title
        self.tvDetallePelicula.text = pelicula.overview
        self.lbTituloOriginal.text = pelicula.original_title
        self.lblFechaLanzamiento.text = pelicula.release_date
        self.lbPopularidad.text = String(format: "%.01f", pelicula.popularity)
        self.lbPromedio.text = String(format: "%f",pelicula.vote_average)
        self.lbTotalVotos.text = String(format: "%d",pelicula.vote_count)
    }
}
