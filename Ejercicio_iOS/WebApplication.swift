//
//  WebApplication.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 14/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class WebApplication: NSObject {

    var url : String
    var respuesta : Data
    var error : String
    
    override init() {
        self.url = ""
        self.respuesta = Data()
        self.error = ""
    }
    
    func consultarGET(metodo:String, parametro : String , completion : @escaping (Data, String) -> () ){
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        let session = URLSession(configuration: sessionConfig)
        
        var urlService = url
        urlService.append(metodo)
        urlService.append(parametro)
        
        guard let url = urlService.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        guard let serviceURL = URL(string: url) else {return}    
        
        let task = session.dataTask(with: serviceURL) { (data, response, error) in
            
            if let responseHTTP = response as? HTTPURLResponse {
                if(responseHTTP.statusCode != 200){
                    self.error = "No cuentas con conexión a internet"
                    completion(self.respuesta, self.error)
                    return
                }
            }
            
            guard let respuestaWS = data else {return}
            completion(respuestaWS , self.error)
        }
        task.resume()
    }
    
    func consultarGET(path : String) -> Data{
        guard let url = URL.init(string: self.url) else {return Data()}
        let urlCompletaPortada = url.appendingPathComponent(path)
        var respData = Data()
        do {
            respData = try Data(contentsOf: urlCompletaPortada)
        } catch  {
            print(error)
        }
        return respData
    }
    
}
