//
//  Utilerias.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 16/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class Utilerias: NSObject {
    
    static func obtenerDocumentsDirectory() -> String{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return paths[0]
    }
    
    static func crearDirectorio(){
        let docURL = URL(string: obtenerDocumentsDirectory())!
        let dataPath = docURL.appendingPathComponent("PortadasPeliculas")
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription);
            }
        }
    }
    
    static func guardarPortadas(path : String, imagen : UIImage){
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileURL = documentDirectory.appendingPathComponent(path)
            if let imageData = imagen.jpegData(compressionQuality: 0.5){
                try imageData.write(to: fileURL)
            }
        } catch  {
            print(error)
        }
    }
    
    static func existeArchivo(path : String) -> Bool{
        let fileManager = FileManager.default
        var docURL = obtenerDocumentsDirectory()
        docURL.append(path)
        return fileManager.fileExists(atPath: docURL)
    }    
}
