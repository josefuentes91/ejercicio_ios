//
//  VistaPRincipalPresenter.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 14/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class VistaPRincipalPresenter: NSObject {
    
    let vistaPrincipalModel = VistaPRincipalModel()
    
    func obtenerTopPelicuasPresenter(completion : @escaping(Array<Pelicula.results>, String) -> ()){
        vistaPrincipalModel.obtenerTopPeliculasModel { (peliculas, error) in
            completion(peliculas, error)
        }
    }
    
    func obtenerInformacionGuardadaPresenter(completion : @escaping(Array<Pelicula.results>) ->()){
        vistaPrincipalModel.obtenerInformacionGuardada { (peliculas) in
            completion(peliculas)
        }
    }
    
    func validarTiempoCargaPresenter() -> Bool{
        return vistaPrincipalModel.validarTiempoCargaInformacion()
    }
    
}
