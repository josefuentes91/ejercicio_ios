//
//  VistaPRincipalModel.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 14/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class VistaPRincipalModel: WebApplication {
    
    var userDefault : UserDefaults
    
    override init(){
        self.userDefault = UserDefaults.standard
        super.init()
        self.url = "https://api.themoviedb.org/3/movie/"
    }
    
    func obtenerTopPeliculasModel(completion : @escaping (Array<Pelicula.results>, String) -> ()){
        
        var parametros = "?api_key=b39275812a66d81ef3f15edb57adaa27"
        parametros.append("&language=es-ES")
        parametros.append("&page=1")
        
        self.consultarGET(metodo: "top_rated", parametro: parametros) { (respuesta, error) in
            self.guardarInformacion(data: respuesta)
            self.userDefault.setValue(Date(), forKey: "fechaCargaInformacion")
            completion(self.procesarInformacion(data: respuesta), error)
        }
    }
    
    func procesarInformacion(data : Data) -> [Pelicula.results] {
        let pelicula = try? JSONDecoder().decode(Pelicula.rootResponse.self, from: data)
        guard let peliculas = pelicula?.results else {return []}
        
        let orderPeliculas = peliculas.sorted { (valor1, valor2) -> (Bool) in
            return((valor1.vote_average == valor2.vote_average) && valor1.vote_count < valor2.vote_count)
        }
        
        for pelicula in orderPeliculas {
            if(!Utilerias.existeArchivo(path: String(format: "/PortadasPeliculas/poster_path_%d.jpg", pelicula.id)) && !Utilerias.existeArchivo(path: String(format: "/PortadasPeliculas/backdrop_path_%d.jpg", pelicula.id))){
                self.obtenerImagenesPelicula(pelicula: pelicula)
            }
        }
        return orderPeliculas
    }
    
    func obtenerImagenesPelicula(pelicula : Pelicula.results){
        
        self.url = "https://image.tmdb.org/t/p/w500/"
        
        var dataRespuesta = consultarGET(path: pelicula.poster_path)
        guard let photoPoster = UIImage.init(data: dataRespuesta) else {return}
        Utilerias.guardarPortadas(path: String(format: "/PortadasPeliculas/poster_path_%d.jpg", pelicula.id), imagen: photoPoster)
        
        dataRespuesta = consultarGET(path: pelicula.backdrop_path)
        guard let photoBackdrop = UIImage.init(data: dataRespuesta) else {return}
        Utilerias.guardarPortadas(path: String(format: "/PortadasPeliculas/backdrop_path_%d.jpg", pelicula.id), imagen: photoBackdrop)
    }
    
    func guardarInformacion(data : Data){
        userDefault.set(data, forKey: "peliculas")
    }
    
    func obtenerInformacionGuardada(completion : @escaping (Array<Pelicula.results>) -> ()){
        let peliculasRecuperadas = userDefault.data(forKey: "peliculas")
        if let dataGuardada = peliculasRecuperadas {
            completion(procesarInformacion(data: dataGuardada))
        }else{
            completion(procesarInformacion(data: Data()))
        }
    }
    
    func validarTiempoCargaInformacion() -> Bool {
        let fechaUltCarga = userDefault.value(forKey: "fechaCargaInformacion") as! Date
        let gregorian = Calendar.current
        let result = gregorian.dateComponents([.hour], from: fechaUltCarga, to: Date())
        guard let diferencia = result.hour else {return false}
        return diferencia >= 24
    }
    
}
