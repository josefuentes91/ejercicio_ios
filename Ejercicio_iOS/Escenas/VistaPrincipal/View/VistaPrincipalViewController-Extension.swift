//
//  VistaPrincipalViewController-Extension.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 16/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

extension VistaPrincipalViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PeliculaCollectionViewCell", for: indexPath)
        
        if let cellProducto = cell as? PeliculaCollectionViewCell{
        if let layout = cvPeliculas?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 20
            layout.minimumInteritemSpacing = 0
            layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 15, right: 20)
            let size = CGSize(width: (self.cvPeliculas.bounds.width/2) - 30, height: 291)
            layout.itemSize = size
            
            if(self.peliculas.count > 0){
                cellProducto.configurarConPelicula(pelicula: self.peliculas[indexPath.row], top: indexPath.row + 1)
            }
            
        }
        return cellProducto
        }
        
        return cell
    }
}

extension VistaPrincipalViewController : UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detallePelicula = DetallePeliculaViewController()
        detallePelicula.pelicula = self.peliculas[indexPath.row]
        
        self.navigationController?.pushViewController(detallePelicula, animated: true)
    }
    
}
