//
//  ViewController-Extension.swift
//  Buscando
//
//  Created by Jose Fuentes on 26/02/20.
//  Copyright © 2020 jose fuentes. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func presentViewController(viewController : UIViewController , animated : Bool , animation : UIView.AnimationOptions){
        viewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        if(animated){
            UIView.transition(with:self.view, duration: 0.7, options:animation, animations: {
                self.presentVC(vc: viewController)
            }, completion: nil)
        }
        else{
            presentVC(vc: viewController)
        }
    }
    
    func presentVC(vc : UIViewController) {
        self.addChild(vc)
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    func habilitarModoOscuro(habilitar : Bool){
        if(habilitar){
            if #available(iOS 13.0, *){
                overrideUserInterfaceStyle = .dark
            }
            else{
                overrideUserInterfaceStyle = .unspecified
            }
        }
    }
    
    func configurarVistaBackGround(activarVista : Bool , mensaje : String){
        if(activarVista){
            //Creamos la vista BackGround
            let viewBackGround = UIView(frame:UIScreen.main.bounds)
            viewBackGround.tag = 701
            viewBackGround.backgroundColor = UIColor.init(red: 44/255, green: 62/255, blue: 80/255, alpha: 1)
            
            //Creamos la etiqueta con descripcion
            let lblMensaje = UILabel()
            lblMensaje.text = mensaje
            lblMensaje.textColor = .white
            lblMensaje.numberOfLines = 2
            lblMensaje.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            lblMensaje.textAlignment = .center
            lblMensaje.frame.size = CGSize(width: viewBackGround.frame.width, height: 50)
            lblMensaje.frame.origin = CGPoint(x: 0, y: view.frame.size.height / 2)
            
            let spCargando = UIActivityIndicatorView()
            spCargando.color = .white
            spCargando.frame.origin = CGPoint(x: view.frame.size.width / 2, y:lblMensaje.frame.origin.y + 100 )
            spCargando.startAnimating()
            
            //Agregamos los elementos a la vista
            viewBackGround.addSubview(spCargando)
            viewBackGround.addSubview(lblMensaje)
            self.view.addSubview(viewBackGround)
        }
        else
        {
            //Eliminamos por tag la vista que se coloco
            UIView.transition(with:self.view, duration: 0.8, options:.transitionCurlUp, animations: {
                self.view.viewWithTag(701)?.removeFromSuperview()
            }, completion: nil)
        }
    }
}
