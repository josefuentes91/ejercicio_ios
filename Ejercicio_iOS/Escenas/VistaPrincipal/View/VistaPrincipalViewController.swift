//
//  VistaPrincipalViewController.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 14/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class VistaPrincipalViewController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var cvPeliculas: UICollectionView!
    
    //Variables
    var peliculas = [Pelicula.results]()
    let vistaPrincipalPresenter = VistaPRincipalPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(_:)), name: UIScene.willEnterForegroundNotification, object: nil)
        
        Utilerias.crearDirectorio()
        configurarVista()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Top Rates Movies"
        obtenerInformacion()
    }
    
    func obtenerInformacion(){
        vistaPrincipalPresenter.obtenerInformacionGuardadaPresenter { (respuesta) in
            self.peliculas = respuesta
            
            if(self.peliculas.count == 0){
                self.obtenerInformacionWS()
            }else{
                self.cvPeliculas.reloadData()
            }
        }
    }
    
    func obtenerInformacionWS(){
        self.configurarVistaBackGround(activarVista: true, mensaje: "Obteniendo información \nespere un momento...")
        
        self.vistaPrincipalPresenter.obtenerTopPelicuasPresenter { (peliculas, error) in
            self.peliculas = peliculas
            
            DispatchQueue.main.async {
                if(self.peliculas.count > 0){
                    self.cvPeliculas.reloadData()
                    self.configurarVistaBackGround(activarVista: false, mensaje: "")
                }
            }
        }
    }
    
    func configurarVista(){
        self.cvPeliculas.register(UINib.init(nibName: "PeliculaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PeliculaCollectionViewCell")
    }
    
    @objc func willEnterForeground(_ notification: Notification){
        if(vistaPrincipalPresenter.validarTiempoCargaPresenter()){
            obtenerInformacionWS()
        }
        
    }
    
}
