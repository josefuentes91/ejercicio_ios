//
//  Peliculas.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 14/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit



class Pelicula {
    
    class rootResponse : Decodable {
        var page : Int
        var results : [results]
        var total_pages : Int
        var total_results : Int
    }
    
    class results : Decodable {
        var backdrop_path : String
        var id : Int
        var original_language : String
        var original_title : String
        var overview : String
        var popularity : Float
        var poster_path : String
        var release_date : String
        var title : String
        var vote_average : Float
        var vote_count : Int
        
        init(){
            backdrop_path = ""
            id = 0
            original_language = ""
            original_title = ""
            overview = ""
            popularity = 0
            poster_path = ""
            release_date = ""
            title = ""
            vote_average = 0
            vote_count = 0
        }
    }
}
