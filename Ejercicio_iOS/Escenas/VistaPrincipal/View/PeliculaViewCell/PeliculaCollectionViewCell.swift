//
//  PeliculaCollectionViewCell.swift
//  Ejercicio_iOS
//
//  Created by Jose Fuentes on 16/10/21.
//  Copyright © 2021 Jose Fuentes. All rights reserved.
//

import UIKit

class PeliculaCollectionViewCell: UICollectionViewCell {

    //IBOutlets
    @IBOutlet weak var ivImagenPelicula: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbTotalVotos: UILabel!
    @IBOutlet weak var lblPromedio: UILabel!
    @IBOutlet weak var lbTop: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configurarConPelicula(pelicula : Pelicula.results, top : Int){
        
        lbTitulo.text = pelicula.title
        lbTotalVotos.text = "Votos: \(pelicula.vote_count)"
        lblPromedio.text = "Promedio: \(pelicula.vote_average)"
        lbTop.text = "\(top)"
        
        var pathImage = Utilerias.obtenerDocumentsDirectory()
        pathImage.append(String(format: "/PortadasPeliculas/poster_path_%d.jpg", pelicula.id))
        ivImagenPelicula.image = UIImage.init(contentsOfFile: pathImage)
    }

}
